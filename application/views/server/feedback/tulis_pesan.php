<?php 
    if($this->session->userdata('role') !== 'admin')  {
    Redirect('Page/pageNotFound');
    } 
?>
          <!-- /.col -->
        <div class="col-md-9">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">Baca Pesan</h3>
            </div>
            <!-- /.card-header -->
              <form method="POST" action="<?= base_url('Feedback/insertSentEmail') ?>">
            <div class="card-body p-0">
              <div class="form-group mx-2 mt-2">
                 <div class="input-group">
                    <span class="input-group-prepend">
                        <label class="input-group-text">Untuk :</label>
                    </span>
                    <input type="email" class="form-control form-control-bold" placeholder="*******@gmail.com" name="email" value="<?php if($this->uri->segment('3') == ""){echo "";}else{echo $this->uri->segment('3')."@gmail.com";} ?>" required>
                </div>
              </div>
              <div class="form-group mx-2">
                  <div class="input-group">
                    <span class="input-group-prepend">
                        <label class="input-group-text">Subject :</label>
                    </span>
                    <input type="text" class="form-control form-control-bold" placeholder="Subject email" name="subject">
                </div>
              </div>
              <div class="ml-4 mr-4 mt-2">
                <div class="row">
                    <div class="card">
                        <textarea class="form-control html-editor" id="summernote" rows="10" name="isi"></textarea>
                    </div>
                </div>
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="float-right">
                <button type="submit" class="btn tema-biru text-white"><i class="far fa-envelope"></i> Kirim</button>
              </div>
              <button type="reset" class="btn btn-default"><i class="fas fa-times"></i><a href="<?= base_url('Feedback') ?>">Batal</a></button>
            </div>
             </form>
            <!-- /.card-footer -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
<script>
  $(document).ready(function() {
  $('#summernote').summernote({
  toolbar: [
      // [groupName, [list of button]]
      ['style', ['style']],
      ['font', ['bold', 'italic', 'underline', 'clear']],
      ['fontname', ['fontname']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['height', ['height']],
      ['table', ['table']],
      ['insert', ['link', 'picture', 'hr']],    
      ],
        height: 400,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: true                  // set focus to editable area after initializing summernote
  });
});
</script>